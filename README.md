# pwpt
[![Build Status](https://travis-ci.org/jhxie/pwpt.svg?branch=master)]
(https://travis-ci.org/jhxie/pwpt)

Pthread-related programs very loosely based (most share identical behaviors or
functionalities only) on examples from "Programming with POSIX® Threads",
by David R. Butenhof.

## Build Instructions/Installation
Both GNU Make (plain unix makefiles, not autotools: i.e. autoconf/automake) and
CMake build system are supported, but the GNU Makefiles are written in an
**in-source** self-contained build format with some extra recursively triggered
makefiles scattered across the source tree.

For CMake, both **in-source** and **out-of-source** build are possible;
however the example shown below is an out-of-source build.  
Please feel free to consult CMake's official documentation for HOW-TOs of an
in-source build if that is what you want.  
In the future the actual installation feature may be added for the CMake build
system but not for the GNU Make build system.

### GNU Make
**IMPORTANT**  
For certain linux distributions the linker flag '-lpthread' seems
to fail somehow, so I changed it to '-pthread' instead, you may need to revert
this flag back to '-lpthread' in the makefiles that use this flag if warnings
regarding this is reported.

If you want to build individual sub-projects for each chapter, remember to
**change directory** to where the makefile resides and then type 'make';
for example, to build the binaries for the alarm sub-project of chapter 1,
```bash
cd 1_intro/alarm && make
```
simple as that.

For a one-pass recursive make for the whole project, type
```bash
make
```
at the top level of the project directory.

### CMake
Make an **empty directory** then build all the source files.
```bash
mkdir build && cd build
cmake .. && make
```

## License
Copyright &copy; 2016 Jiahui Xie  
Licensed under the [GNU General Public License v3][GPL3].  
Distributed under the [GNU General Public License v3][GPL3].

[GPL3]: https://opensource.org/licenses/GPL-3.0
