#include "cmnutil.h"

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
        FILE   *filelist = NULL;
        char   *linebuff = NULL;
        size_t  linesize = 0;
        for (int i = 1; i < argc; ++i) {
                CMNUTIL_ERRNOABRT(NULL, filelist = fopen(argv[i], "r"));
                while (-1 != getline(&linebuff, &linesize, filelist)) {
                        printf("%s", linebuff);
                }
        }
        free(linebuff);
        return 0;
}
