/*
 * Copyright © 2016 Jiahui Xie
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ALARM_MUTEX_H
#define ALARM_MUTEX_H

#include <time.h>

/*
 * The 'alarm_info' structure now contains the expiry time (represented by a
 * time_t type, seconds since the Epoch) for each alarm, so they can be sorted.
 * Storing the requested number of seconds would not be enough because the
 * 'alarm_thread' cannot tell how long it has been on the list.
 */
struct alarm_info {
        struct alarm_info *link;
        int seconds;
        time_t expiry;
        char message[1U << 12U];
};

void               *alarm_thread  (void *arg __attribute__((unused)));
long long           sleep_time_get(const char *restrict token);
int main(void);
#endif /* ALARM_MUTEX_H */
