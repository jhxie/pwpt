/*
 * Copyright © 2016 Jiahui Xie
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "alarm_mutex.h"
#include "cmnutil.h"

#include <errno.h>
#include <limits.h>
#include <pthread.h>
#include <sched.h>
#include <string.h>
#include <unistd.h>

static const char *const DELIM = " \t";
struct alarm_info *alarm_list    = NULL;
pthread_mutex_t    alarm_mutex   = PTHREAD_MUTEX_INITIALIZER;

void *alarm_thread(void *arg __attribute__((unused)))
{
        struct alarm_info *alarm;
        unsigned sleep_time;
        time_t now;

        /*
         * Loop forever, processing commands.
         * The alarm thread would be disintegrated when the process exits.
         */
        while (1) {
                CMNUTIL_ERRABRT(pthread_mutex_lock(&alarm_mutex));
                alarm = alarm_list;

                /*
                 * If the alarm list is empty, wait for one second.
                 * This allows the main thread to run, and read another command.
                 */
                if (NULL == alarm) {
                        sleep_time = 1U;
                /*
                 * If the list is not empty, remove the first item.
                 * Compute the number of seconds to wait -- if the result is
                 * less than 0 (the time has passed), then set the 'sleep_time'
                 * to 0.
                 */
                } else {
                        alarm_list = alarm->link;
                        now = time(NULL);

                        if (now >= alarm->expiry) {
                                sleep_time = 0;
                        } else {
                                sleep_time = alarm->expiry - now;
                        }
#ifdef DEBUG
                        struct tm tmp_date_ = { 0 };
                        char      timebuf_[1 << 12] = { '\0' };
                        CMNUTIL_ERRNOABRT(NULL,
                                          localtime_r(&alarm->expiry,
                                                      &tmp_date_));
                        CMNUTIL_ERRNOABRT(0,
                                          strftime(timebuf_,
                                                   sizeof timebuf_,
                                                   "%a %b %d %T %Z %Y",
                                                   &tmp_date_));
                        printf("[Waiting] At [%s] Print [%s]\n",
                               timebuf_,
                               alarm->message);
#endif
                }
                /*
                 * Unlock the mutex before waiting, so that the main thread
                 * can lock it to insert a new alarm request.
                 * If the sleep_time is 0, then call 'sched_yield()',
                 * giving the main thread a chance to run if it has been
                 * readied by user input, without delaying the message
                 * if there is no input.
                 */
                CMNUTIL_ERRABRT(pthread_mutex_unlock(&alarm_mutex));
                if (0U < sleep_time) {
                        unsigned remain = sleep_time;
                        do {
                                remain = sleep(remain);
                        } while (0U != remain);
                } else {
                        CMNUTIL_ERRABRT(sched_yield());
                }
                /*
                 * If a timer expired, print the message and free the structure.
                 */
                if (NULL != alarm) {
                        printf("[%u] %s\n", alarm->seconds, alarm->message);
                        free(alarm);
                }
        }
        return NULL;
}

int main(void)
{
        struct alarm_info  *alarm;
        struct alarm_info  *next;
        struct alarm_info **last;
        char               *line      = NULL;
        char               *saveptr   = NULL;
        char               *token     = NULL;
        size_t              line_size = 0;
        pthread_t           thread;

        CMNUTIL_ERRABRT(pthread_create(&thread, NULL, alarm_thread, NULL));

        while (1) {
                printf("Alarm> ");

                if (-1 == getline(&line, &line_size, stdin)) {
                        break;
                }

                if (1U >= strlen(line)) {
                        continue;
                }
                CMNUTIL_ERRNOABRT(NULL,
                                  alarm = calloc(1, sizeof(struct alarm_info)));

                if (NULL == (token = strtok_r(line, DELIM, &saveptr))) {
                        continue;
                }
                if (-1 == (alarm->seconds = sleep_time_get(token))) {
                        free(alarm);
                        continue;
                }
                /*
                 * Check whether the message to be displayed has size less
                 * than 2 ^ 12, which is done by checking the next token
                 * 'saveptr'.
                 */
                if ((1 << 12) < strlen(saveptr)) {
                        free(alarm_list);
                        continue;
                }

                CMNUTIL_ERRABRT(pthread_mutex_lock(&alarm_mutex));
                strncpy(alarm->message, saveptr, strlen(saveptr));
                alarm->expiry = time(NULL) + alarm->seconds;
                /*
                 * Insert the new alarm into the list of alarms sorted by
                 * expiration time.
                 */
                last = &alarm_list;
                next = *last;
                while (NULL != next) {
                        if (next->expiry >= alarm->expiry) {
                                alarm->link = next;
                                *last = alarm;
                                break;
                        }
                        last = &(next->link);
                        next = next->link;
                }
                /*
                 * If we reached the end of the list, insert the new alarm there.
                 * ('next' is NULL, and 'last' points to the link field of the
                 *  last item, or to the list header)
                 */
                if (NULL == next) {
                        *last = alarm;
                        alarm->link = NULL;
                }
                CMNUTIL_ERRABRT(pthread_mutex_unlock(&alarm_mutex));
        }
        free(line);
        return 0;
}

long long sleep_time_get(const char *restrict token)
{
        char          *endptr = NULL;
        errno                 = 0;
        unsigned       result = 0;
        unsigned long  tmp    = 0UL;

        /*
         * the first token cannot be converted to an unsigned long
         */
        if (0UL == (tmp = strtoul(token, &endptr, 10)) && endptr == token) {
                return -1;
        }
        /*
         * the result overflows an unsigned long
         */
        if (ULONG_MAX == tmp && ERANGE == errno) {
                return -1;
        }
        result = tmp;
        /*
         * the results overflows an unsigned int
         */
        if ((unsigned long)result != tmp) {
                return -1;
        }
        return result;
}
