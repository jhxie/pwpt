/*
 * Copyright © 2016 Jiahui Xie
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CMNUTIL_H
#define CMNUTIL_H


/*
                       +-------------------------------+
                       |Compiler/Libc Test Conditionals|
                       +-------------------------------+
*/
#include <features.h>
/*
 * For gcc, c99 is fully supported since 4.3, so a test is performed
 * in the following.
 *
 * To avoid erroneous stop of compilation behavior from llvm-clang,
 * an extra macro is tested as well due to the fact that clang also
 * defines __GNUC__ if invoked with "-std=gnu99" flag.
 *
 * A similar test can be performed for clang, but the technique is
 * not known for the author(jiahui) of this software so it is left out,
 * but as long as you are not using some ancient versions you should be fine.
 *
 * Reference
 * https://gcc.gnu.org/c99status.html
 */
#if !defined(__clang__) && defined(__GNUC__)
#if 4 > __GNUC__
#error "<" __FILE__ "> GCC Version Test -- [ FAIL ]"
#error "This software requires gcc to be at least 4.3"
#elif 4 == __GNUC__ && 3 > __GNUC_MINOR__
#error "<" __FILE__ "> GCC Version Test -- [ FAIL ]"
#error "This software requires gcc to be at least 4.3"
#endif
#endif

/* secure_getenv first appeared in glibc 2.17 */
#if defined(__GLIBC__)
#if 2 > __GLIBC__
#error "<" __FILE__ "> GLIBC Version Test -- [ FAIL ]"
#error "This software requires the glibc to be at least 2.17"
#elif 2 == __GLIBC__ && 17 > __GLIBC_MINOR__
#error "<" __FILE__ "> GLIBC Version Test -- [ FAIL ]"
#error "This software requires the glibc to be at least 2.17"
#endif
#endif


/*
                             +-------------------+
                             |Feature Test Macros|
                             +-------------------+
*/
#define _POSIX_C_SOURCE 200809L


/*
                                +--------------+
                                |Utility Macros|
                                +--------------+
*/
#include <errno.h>  /* errno */
#include <stdio.h>  /* fprintf() */
#include <stdlib.h> /* abort() */
#include <string.h> /* strerror() */

#define CMNUTIL_ERRABRT(expr) \
        do { \
                int status__ = 0; \
                if (0 != (status__ = (expr))) { \
                        fprintf(stderr, \
                                "[%s] on line [%d] within function [%s]" \
                                "in file [%s]: %s\n", \
                                #expr, __LINE__, __func__, \
                                __FILE__, strerror(status__)); \
                        abort(); \
                } \
        } while (0)


#define CMNUTIL_ERRNOABRT(experr, expr) \
        do { \
                if ((experr) == (expr)) { \
                        fprintf(stderr, \
                                "[%s] on line [%d] within function [%s]" \
                                "in file [%s]: %s\n", \
                                #expr, __LINE__, __func__, \
                                __FILE__, strerror(errno)); \
                        abort(); \
                } \
        } while (0)

#define CMNUTIL_STREACH(iterator_, ...) \
        for (char **iterator_ = (char *[]){__VA_ARGS__, NULL}; \
             *iterator_; \
             ++iterator_)

#define CMNUTIL_ZFREE(ptr) \
        do { \
                free(ptr); \
                ptr = NULL; \
        } while (0)


#endif /* CMNUTIL_H */
