/*
 * Copyright © 2016 Jiahui Xie
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cmnutil.h"

#include <pthread.h>
#include <stdio.h>

void *thread_routine(void *restrict arg)
{
        /*
         * Suggestion given by the reference book, quoted verbatim:
         * "It is a good idea for all thread functions to return something,
         * even if it is simply NULL.
         * If you omit the return statement, pthread_join will still return
         * some value - whatever happens to be in the place where the
         * thread’s start function would have stored a return value."
         */
        return arg;
}

/*
 * The program creates a thread by calling pthread_create() and then waits for
 * it by calling pthread_join().
 * 
 * Note that the wait is purely optional; however, returning from main()
 * function would cause the process to terminate along with all threads.
 * 
 * A better alternative is to call pthread_exit(), which would allow the process
 * to wait until all threads have terminated.
 */
int main(void)
{
        pthread_t thread_id;
        void *thread_result;

        CMNUTIL_ERRABRT(pthread_create(&thread_id, NULL, thread_routine, NULL));
        /*
         * Again, from the reference book quoted verbatim:
         * "The pthread_join function will block the caller until the thread you
         * specify has terminated, and then, optionally, store the terminated
         * thread’s return value."
         * 
         * Here it is stored in the variable 'thread_result'.
         */
        CMNUTIL_ERRABRT(pthread_join(thread_id, &thread_result));

        /*
         * When the join completes, the program checks the thread's return value
         * to be sure that the thread returned the value it is given (in this
         * case NULL).
         */
        puts((NULL == thread_result) ? "EXIT_SUCCESS" : "EXIT_FAILURE");
        return 0;
}
