/*
 * Copyright © 2016 Jiahui Xie
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cmnutil.h"

#include <errno.h>
#include <limits.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static const char *const DELIM = " \t";

void wait_msg(char *restrict line) __attribute__((noreturn));

int main(void)
{
        char *line = NULL;
        size_t line_size = 0;
        struct sigaction sa = {
                .sa_handler = SIG_IGN,
                .sa_flags = SA_NOCLDSTOP | SA_NOCLDWAIT
        };

        /*
         * Make sure the children do not become zombies
         * since the parent does not wait for them after forking.
         */
        if (-1 == sigemptyset(&sa.sa_mask)) {
                exit(EXIT_FAILURE);
        }

        if (-1 == sigaction(SIGCLD, &sa, NULL)) {
                exit(EXIT_FAILURE);
        }

        while (1) {
                printf("Alarm> ");

                if (-1 == getline(&line, &line_size, stdin)) {
                        break;
                }

                switch (fork()) {
                case -1:
                        goto main_exit;
                /*
                 * Let children do the actual work
                 */
                case 0:
                        wait_msg(line);
                default:
                        break;
                }
        }

main_exit:
        free(line);
        return 0;
}

void wait_msg(char *restrict line)
{
        unsigned seconds = 0;
        char *saveptr = NULL;
        char *token = NULL;

        if (1U >= strlen(line)) {
                goto wait_msg_exit;
        }

        if (NULL == (token = strtok_r(line, DELIM, &saveptr))) {
                goto wait_msg_exit;
        }
        /*
         * now token is the sleep time
         */
        {
                char *endptr = NULL;
                unsigned long tmp = 0UL;
                errno = 0;

                /*
                 * the first token cannot be converted to an
                 * unsigned long
                 */
                if (0UL == (tmp = strtoul(token, &endptr, 10)) &&
                    endptr == token) {
                        goto wait_msg_exit;
                }

                /*
                 * the result overflows an unsigned long
                 */
                if (ULONG_MAX == tmp && ERANGE == errno) {
                        goto wait_msg_exit;
                }

                seconds = tmp;

                /*
                 * the results overflows an unsigned int
                 */
                if ((unsigned long)seconds != tmp) {
                        goto wait_msg_exit;
                }
        }

        /*
         * now saveptr is the message to be displayed upon expiration
         */
        {
                unsigned remaining = seconds;
                do {
                        remaining = sleep(remaining);
                } while (0 != remaining);
        }
        printf("(%u) %s\n", seconds, saveptr);

wait_msg_exit:
        free(line);
        exit(EXIT_SUCCESS);
}
