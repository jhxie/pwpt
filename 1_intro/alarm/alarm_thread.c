/*
 * Copyright © 2016 Jiahui Xie
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cmnutil.h"

#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>

static const char *const DELIM = " \t";

void *wait_msg(void *arg);

int main(void)
{
        char *line = NULL;
        char *line_copy = NULL;
        size_t line_size = 0;

        /*
         * all the parsing is done in the wait_msg() function, which differs
         * from the sample version given in the book, in this case a separate
         * bookkeeping structure like alarm_t containing seconds and message
         * is not necessary
         */
        while (1) {
                printf("Alarm> ");

                if (-1 == getline(&line, &line_size, stdin)) {
                        break;
                }

                if (NULL == (line_copy = strdup(line))) {
                        goto main_exit;
                }

                /*
                 * for the master/boss thread the identity of its slaves/workers
                 * is not important, so an anonymous compound literal is used
                 */
                if (0 != pthread_create(((pthread_t []){ 0 }),
                                        NULL,
                                        wait_msg,
                                        line_copy)) {
                        goto main_exit;
                }
        }

main_exit:
        free(line);
        return 0;
}

/*
 * This function expects a copy of the argument from the heap;
 * i.e. it *does* *not* share variables with its caller.
 */
void *wait_msg(void *arg)
{
        char *line = (char *)arg;
        unsigned seconds = 0;
        char *saveptr = NULL;
        char *token = NULL;

        /*
         * detaches itself so the caller does not need to worry about its
         * return value: pthread run time can immediately free the resource
         * allocated for each instance of the running thread of this function
         * upon termination
         */
        if (0 != pthread_detach(pthread_self())) {
                goto wait_msg_exit;
        }

        if (1U >= strlen(line)) {
                goto wait_msg_exit;
        }

        if (NULL == (token = strtok_r(line, DELIM, &saveptr))) {
                goto wait_msg_exit;
        }
        /*
         * now token is the sleep time
         */
        {
                char *endptr = NULL;
                unsigned long tmp = 0UL;
                errno = 0;

                /*
                 * the first token cannot be converted to an
                 * unsigned long
                 */
                if (0UL == (tmp = strtoul(token, &endptr, 10)) &&
                    endptr == token) {
                        goto wait_msg_exit;
                }

                /*
                 * the result overflows an unsigned long
                 */
                if (ULONG_MAX == tmp && ERANGE == errno) {
                        goto wait_msg_exit;
                }

                seconds = tmp;

                /*
                 * the results overflows an unsigned int
                 */
                if ((unsigned long)seconds != tmp) {
                        goto wait_msg_exit;
                }
        }

        /*
         * now saveptr is the message to be displayed upon expiration
         */
        {
                unsigned remaining = seconds;
                do {
                        remaining = sleep(remaining);
                } while (0 != remaining);
        }
        printf("(%u) %s\n", seconds, saveptr);

wait_msg_exit:
        free(line);
        return NULL;
}
