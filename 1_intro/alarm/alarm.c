/*
 * Copyright © 2016 Jiahui Xie
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cmnutil.h"

#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static const char *const DELIM = " \t";

int main(void)
{
        unsigned seconds = 0;
        char *line = NULL;
        char *saveptr = NULL;
        char *token = NULL;
        size_t line_size = 0;

        while (1) {
                printf("Alarm> ");

                if (-1 == getline(&line, &line_size, stdin)) {
                        break;
                }

                if (1U >= strlen(line)) {
                        continue;
                }

                if (NULL == (token = strtok_r(line, DELIM, &saveptr))) {
                        continue;
                }
                /*
                 * now token is the sleep time
                 */
                {
                        char *endptr = NULL;
                        unsigned long tmp = 0UL;
                        errno = 0;

                        /*
                         * the first token cannot be converted to an
                         * unsigned long
                         */
                        if (0UL == (tmp = strtoul(token, &endptr, 10)) &&
                            endptr == token) {
                                continue;
                        }

                        /*
                         * the result overflows an unsigned long
                         */
                        if (ULONG_MAX == tmp && ERANGE == errno) {
                                continue;
                        }

                        seconds = tmp;

                        /*
                         * the results overflows an unsigned int
                         */
                        if ((unsigned long)seconds != tmp) {
                                continue;
                        }
                }

                /*
                 * now saveptr is the message to be displayed upon expiration
                 */
                {
                        unsigned remaining = seconds;
                        do {
                                remaining = sleep(remaining);
                        } while (0 != remaining);
                }
                printf("(%u) %s\n", seconds, saveptr);
        }

        free(line);
        return 0;
}
