#------------------------- GLOBAL MAKEFILE VARIABLEs ---------------------------
export CC     := gcc
export CFLAGS := -std=gnu99 -Wall -O0 -ftrapv -fsanitize=undefined \
	-pthread -ggdb3 -pipe
#-------------------------------------------------------------------------------


#------------------------------- PHONY TARGETs ---------------------------------
all: alarm life_cycle alarm_mutex

clean:
	$(MAKE) clean -C 1_intro/alarm/
	$(MAKE) clean -C 2_threads/life_cycle/
	$(MAKE) clean -C 3_synchronization/alarm_mutex/

cscope:
	-rm -f cscope*
	-cscope -RqI include/
#-------------------------------------------------------------------------------


#-------------------------------- FILE TARGETs ---------------------------------
alarm:
	$(MAKE) -C 1_intro/alarm/

life_cycle:
	$(MAKE) -C 2_threads/life_cycle/

alarm_mutex:
	$(MAKE) -C 3_synchronization/alarm_mutex/
#-------------------------------------------------------------------------------


#----------------------------- SPECIAL DIRECTIVEs ------------------------------
.PHONY: all clean cscope
.IGNORE:
#-------------------------------------------------------------------------------
